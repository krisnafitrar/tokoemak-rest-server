<?php
defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Pengaturan extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Pengaturan', 'pengaturan');
    }

    public function index_get()
    {
        $act = $this->get('act');
        $key1 = $this->get('key1');
        $key2 = $this->get('key2');

        switch ($act) {
            case 'getlatlong':
                $latitude = $this->pengaturan->getByKey($key1);
                $longitude = $this->pengaturan->getByKey($key2);

                $this->response([
                    'status' => true,
                    'latitude' => $latitude['valuepengaturan'],
                    'longitude' => $longitude['valuepengaturan']
                ], REST_Controller::HTTP_OK);
                break;
            case 'getbykey':
                $value = $this->pengaturan->getByKey($key1);

                $this->response([
                    'status' => true,
                    'value' => $value['valuepengaturan']
                ], REST_Controller::HTTP_OK);
                break;
            case 'get':
                $data = $this->pengaturan->get();
                $this->response($data);
                break;
        }
    }
}
