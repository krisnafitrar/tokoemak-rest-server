<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Kategori extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Kategori');
    }

    public function index_get()
    {
        $idrole = $this->get('idrole');
        $data = $this->M_Kategori->getKategori($idrole);

        if ($data) {
            $this->response($data);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}
