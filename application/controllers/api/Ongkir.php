<?php
defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Ongkir extends REST_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Ongkir','ongkir');
    }

    public function index_get()
    {
    	$key = $this->get('idongkir');

    	if(!empty($key)){
    		$this->response($this->ongkir->getByKey($key)['biaya']);
    	}else{
    		$this->response($this->ongkir->get());
    	}

    }


}