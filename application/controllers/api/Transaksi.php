<?php
defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Transaksi extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_Transaksi', 'transaksi');
    }

    public function index_get()
    {
    }

    public function index_post()
    {
        $act = $this->post('act');
        $idtransaksi = $this->post('idtransaksi');
        $iduser = $this->post('iduser');
        $time = time();
        $jadwalkirim = $this->post('jadwalkirim');
        $alamat = $this->post('alamat');
        $estimasi = $this->post('estimasi');
        $ongkir = $this->post('ongkir');
        $diskon = $this->post('diskon');
        $total = $this->post('total');
        $status = 1;
        $latitude = $this->post('latitude');
        $longitude = $this->post('longitude');
        $idbarang = $this->post('idbarang');
        $jumlah = $this->post('jumlah');
        $idstatus = $this->post('idstatus');
        $waktu = date('j-M-Y, H:i');
        $params = $this->post('params');

        $transaksi = [
            ''
        ];
    }
}
