<?php

class M_Produk extends CI_Model
{

    public function getProdukByKey($key)
    {
        $q = "SELECT b.namabarang,b.harga,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang jb USING(idjenis) JOIN merk USING(idmerk) WHERE b.idkategori='$key'";
        $get = $this->db->query($q);

        return $get;
    }

    public function searchProdukByKeyword($keyword, $key)
    {
        $q = "SELECT b.namabarang,b.harga,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang jb USING(idjenis) JOIN merk USING(idmerk) WHERE b.namabarang LIKE '%$keyword%' AND b.idkategori='$key'";
        $get = $this->db->query($q);

        return $get;
    }

    public function sortProdukByKey($key, $typesort)
    {
        $q = "SELECT b.namabarang,b.harga,b.diskon,b.stok,b.idkategori,b.gambar,b.deskripsi,jb.namajenis FROM barang b JOIN kategori USING(idkategori) JOIN jenisbarang jb USING(idjenis) JOIN merk USING(idmerk) WHERE b.idkategori='$key' ORDER BY b.harga " . $typesort;
        $get = $this->db->query($q);

        return $get;
    }

    public function getProdukByName($nama)
    {
        $val = $this->db->get_where('barang', ['namabarang' => $nama])->row_array();

        return $val;
    }
}
