<?php


class M_Konten extends CI_Model
{

    public function getKontenByKey($key)
    {
        $getKonten = $this->db->get_where('konten', ['idkontenkategori' => $key])->result_array();

        return $getKonten;
    }
}
