<?php

class M_Jenis extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

    public function get()
    {
        return $this->db->get('jenisbarang')->result_array();
    }

    public function getByKey($key)
    {
    	return $this->db->get_where('jenisbarang',['idkategori' => $key])->result_array();
    }
}